# Webform Better Results

This module adds some additional functionality to the standard webform results list.

- Search by Submission ID
- Search entity references by name
- Filter dropdowns for entity reference elements
- Provide hooks for other modules to add to / alter result filters

For a full description of the module, visit the
[project page](https://www.drupal.org/project/webform_better_results).

## Requirements

This module requires the following modules:

- [Webform](https://www.drupal.org/project/webform)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

This overrides the existing WebformSubmissionListBuilder automatically once enabled.

## Contributors

This module contributed by maxwellkeeble on behalf of Students' Union UCL (UCLU).
