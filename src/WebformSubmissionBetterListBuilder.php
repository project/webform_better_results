<?php

namespace Drupal\webform_better_results;

use Drupal;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\webform\WebformEntityStorageInterface;
use Drupal\webform\WebformSubmissionListBuilder;
use Drupal\webform\WebformSubmissionStorageInterface;

/**
 * Provides a list controller for webform submission entity.
 *
 * @ingroup webform
 */
class WebformSubmissionBetterListBuilder extends WebformSubmissionListBuilder {

  /**
   * Initialize WebformSubmissionListBuilder object.
   */
  protected function initialize() {
    parent::initialize();
  }

  /**
   * Get the base entity query filtered by webform and search.
   *
   * @param string $keys
   *   (optional) Search key.
   * @param string $state
   *   (optional) Submission state.
   * @param string $source_entity
   *   (optional) Source entity (type:id).
   *
   * @return QueryInterface
   *   An entity query.
   */
  protected function getQuery($keys = '', $state = '', $source_entity = '') {
    /** @var WebformSubmissionStorageInterface $submission_storage */
    $submission_storage = $this->getStorage();
    $query = $submission_storage->getQuery();
    $submission_storage->addQueryConditions($query, $this->webform, $this->sourceEntity, $this->account);

    // If we are viewing all submissions, we want to exclude
    // any orphaned submissions.
    if (empty($this->webform)) {
      /** @var WebformEntityStorageInterface $webform_storage */
      $webform_storage = $this->entityTypeManager->getStorage('webform');
      $query->condition('webform_id', $webform_storage->getWebformIds(), 'IN');
    }

    // Filter by key(word).
    if ($keys) {
      // Search values.
      $sub_query = Database::getConnection()
        ->select('webform_submission_data', 'sd')
        ->fields('sd', ['sid'])
        ->condition('value', '%' . $keys . '%', 'LIKE');
      $submission_storage->addQueryConditions($sub_query, $this->webform);

      // Search UUID and Notes.
      $or_condition = $query->orConditionGroup();
      $or_condition->condition('notes', '%' . $keys . '%', 'LIKE');
      // Only search UUID if keys is alphanumeric with dashes.
      // @see Issue #2978420: Error SQL with accent mark submissions filter.
      if (preg_match('/^[0-9a-z-]+$/', $keys)) {
        $or_condition->condition('uuid', $keys);
      }

      $query->condition(
        $query->orConditionGroup()
          ->condition('sid', $sub_query, 'IN')
          // If searching for exactly the SID:
          ->condition('sid', $keys, '=')
          ->condition($or_condition)
      );
    }

    // Filter by (submission) state.
    switch ($state) {
      case static::STATE_STARRED:
        $query->condition('sticky', 1);
        break;

      case static::STATE_UNSTARRED:
        $query->condition('sticky', 0);
        break;

      case static::STATE_LOCKED:
        $query->condition('locked', 1);
        break;

      case static::STATE_UNLOCKED:
        $query->condition('locked', 0);
        break;

      case static::STATE_DRAFT:
        $query->condition('in_draft', 1);
        break;

      case static::STATE_COMPLETED:
        $query->condition('in_draft', 0);
        break;
    }

    // Filter by source entity.
    if ($source_entity && strpos($source_entity, ':') !== FALSE) {
      [$entity_type, $entity_id] = explode(':', $source_entity);
      $query->condition('entity_type', $entity_type);
      $query->condition('entity_id', $entity_id);
    }

    // Filter by draft. (Only applies to user submissions and drafts)
    if (isset($this->draft)) {
      // Cast boolean to integer to support SQLite.
      $query->condition('in_draft', (int) $this->draft);
    }

    $queryStrings = Drupal::request()->query->all();

    // Filter by dates
    if (isset($queryStrings['date_start']) && $value = $queryStrings['date_start']) {
      $value = strtotime($value);
      $query->condition('completed', (int) $value, '>=');
    }
    if (isset($queryStrings['date_end']) && $value = $queryStrings['date_end']) {
      $value = strtotime($value);
      $query->condition('completed', (int) $value, '<=');
    }

    // Filter by entity reference fields
    $queryEntityReferences = [];
    foreach ($queryStrings as $key => $value) {
      if (strpos($key, 'entity_reference-') === 0) {
        $queryEntityReferences[str_replace('entity_reference-', '', $key)] = $value;
      }
    }
    foreach ($queryEntityReferences as $element_id => $value) {
      $sub_query = Database::getConnection()
        ->select('webform_submission_data', 'sd')
        ->fields('sd', ['sid'])
        ->condition('name', $element_id)
        ->condition('value', $value, '=');
      $submission_storage->addQueryConditions($sub_query, $this->webform);
      $query->condition('sid', $sub_query, 'IN');
    }

    // Allow hooks to modify query as well:
    $data = [
      'query' => $query,
      'submission_storage' => $submission_storage,
    ];
    Drupal::moduleHandler()
      ->alter('webform_better_results_query', $data, $queryStrings, $this->webform);
    $query = $data['query'];
    $submission_storage = $data['submission_storage'];

    return $query;
  }

  public static function getEntityReferenceElements($webform) {
    $elements = $webform->getElementsDecodedAndFlattened();
    $entityReferenceElements = [];
    foreach ($elements as $key => $element) {
      if (isset($element['#type']) && in_array($element['#type'], [
          'webform_entity_select',
          'webform_entity_autocomplete',
          'entity_autocomplete',
        ])) {
        $entityReferenceElements[$key] = $element;
      }
    }
    return $entityReferenceElements;
  }

}
